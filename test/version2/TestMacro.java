package version2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

import command.Command;
import command.Insert;
import command.Select;
import ihm.Frame;
import ihm.FrameController;
import memento.CareTaker;
import memento.InsertMemento;
import memento.Interval;
import memento.SelectMemento;
import receiver.EngineImpl;

public class TestMacro {

	@Test
	public void testListenMacro() {
		FrameController fc = new FrameController();
		fc.configure();
		CareTaker  c = fc.getCareTaker();
		Frame	   f = fc.getFrame();
		Map<String,Command> commands = f.getCommands();
		f.setFirstIndice(0);
		f.setLastIndice(0);
		f.setText('c');
		c.startRecord();
		commands.get("select").execute();
		commands.get("insert").execute();
		c.stopRecord();
		Select s = ((Select)c.getOriginatorAt(0));
		assertNotNull("Command select null", s);
		Interval i = ((SelectMemento)c.getMementoAt(0)).getValue();
		assertEquals(0, i.getLeftBound());
		assertEquals(0, i.getRightBound());
		Insert ins = ((Insert)c.getOriginatorAt(1));
		assertNotNull("Command insert null", ins);
		char value = ((InsertMemento)c.getMementoAt(1)).getValue().charAt(0);
		assertEquals('c', value);
	}
	
	@Test
	public void testPlayMacro() {
		FrameController fc = new FrameController();
		fc.configure();
		EngineImpl e = fc.getEngine();
		Frame	   f = fc.getFrame();
		CareTaker  c = fc.getCareTaker();
		Map<String,Command> commands = f.getCommands();
		c.startRecord();
		f.setFirstIndice(0);
		f.setLastIndice(0);
		f.setText('c');
		commands.get("select").execute();
		commands.get("insert").execute();
		c.stopRecord();
		f.setFirstIndice(0);
		f.setLastIndice(1);
		f.setText('a');
		commands.get("select").execute();
		commands.get("insert").execute();
		c.replay();
		assertEquals("ca", e.getMainBuffer());
	}
}
