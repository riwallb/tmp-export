package version1;

import static org.junit.Assert.*;

import org.junit.Test;

import ihm.FrameController;
import receiver.EngineImpl;

public class TestEngineImpl {
	
	@Test
	public void testMoteur() {
		FrameController f = new FrameController();
		f.configure();
		EngineImpl e = f.getEngine();
		assertNotNull("Null moteur", e);
		assertNotNull("Null buffer", e.getMainBuffer());
		assertNotNull("Null clipboard", e.getClipboard());
		assertEquals(0, e.getSelectFront());
		assertEquals(0, e.getSelectBack());	
	}

	@Test
	public void testCut() {
		FrameController f = new FrameController();
		f.configure();
		EngineImpl e = f.getEngine();
		e.insert("cou");
		e.select(0, 3);
		e.cut();
		assertEquals("", e.getMainBuffer()); // Le cut doit bien supprimer la sélection
		assertEquals("cou", e.getClipboard()); // Et dans le presse papier nous devons bien avoir "cou"
	}

	@Test
	public void testCopy() {
		FrameController f = new FrameController();
		f.configure();
		EngineImpl e = f.getEngine();
		e.insert("cou");
		e.select(0, 3);
		e.copy();
		assertEquals("cou", e.getMainBuffer()); // Le copy ne doit pas altérer la sélection
		assertEquals("cou", e.getClipboard());
	}

	@Test
	public void testPaste() {
		FrameController f = new FrameController();
		f.configure();
		EngineImpl e = f.getEngine();
		e.insert("cou");
		e.select(0, 3);
		e.cut();			// La chaîne est vide
		e.paste();
		e.paste();
		assertEquals("coucou", e.getMainBuffer());
	}

	@Test
	public void testSelect() {
		FrameController f = new FrameController();
		f.configure();
		EngineImpl e = f.getEngine();
		e.insert("cou");
		e.select(0, 3);
		assertEquals(0, e.getSelectFront());
		assertEquals(3, e.getSelectBack());
		e.select(-10, 10);
		assertEquals(0, e.getSelectFront());
		assertEquals(3, e.getSelectBack());
	}

	@Test
	public void testInsert() {
		FrameController f = new FrameController();
		f.configure();
		EngineImpl e = f.getEngine();
		e.insert("coucou");
		assertEquals("coucou", e.getMainBuffer());
	}
	
}
