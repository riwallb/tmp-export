package memento;

import java.util.ArrayList;
import java.util.Iterator;

import command.Command;

public class CareTaker {
	private ArrayList<Memento> 		savedStats;
	private ArrayList<Originator> 	savedCommands;
	private boolean 					isReplaying;
	private boolean					isRecording;
	
	public CareTaker() {
		savedStats 		= new ArrayList<>();
		savedCommands 	= new ArrayList<>();
		isReplaying		= false;
		isRecording		= false;
	}
	
	public void startRecord() {
		isRecording = true;
	}
	
	public void stopRecord() {
		isRecording = false;
	}
	
	public void replay() {
		if(!isRecording) {
			Iterator<Originator> 	iteratorOrigin 	= savedCommands.iterator();
			Iterator<Memento> 		iteratorStats 	= savedStats.iterator();
			isReplaying = true;
			while(iteratorOrigin.hasNext()) {
				Originator o 	= iteratorOrigin.next();
				Memento m		= iteratorStats.next();
				o.restoreFromMemento(m);
				((Command)o).execute();
			}
			isReplaying = false;
		}
	}
	
	public void addNewCommand(Originator o) {
		savedStats.add(o.saveToMemento());
		savedCommands.add(o);
	}
	
	public void empty() {
		savedStats.clear();
		savedCommands.clear();
	}
	
	public boolean isReplaying() {
		return this.isReplaying;
	}
	
	public boolean isRecording() {
		return this.isRecording;
	}
	
	public Originator getOriginatorAt(int index) { return savedCommands.get(index); }
	public Memento 		getMementoAt(int index) { return savedStats.get(index); }
}
