package memento;

public class SelectMemento implements Memento {
	private Interval myState;
	
	public void setValue(Interval i) {
		this.myState = i;
	}
	
	public Interval getValue() {
		return this.myState;
	}
}
