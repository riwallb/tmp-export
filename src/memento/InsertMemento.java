package memento;

public class InsertMemento implements Memento {
	private String myState;
	
	public void setValue(String s) {
		this.myState = s;
	}
	
	public String getValue() {
		return this.myState;
	}
}
