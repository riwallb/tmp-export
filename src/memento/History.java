package memento;

import java.util.ArrayList;


public class History {
	private ArrayList<HistoryOriginator>	OriginatorRedo 	= new ArrayList<>(); 
	private ArrayList<HistoryMemento>	MementoRedo	 	= new ArrayList<>();
	private ArrayList<HistoryOriginator>	OriginatorUndo 	= new ArrayList<>();
	private ArrayList<HistoryMemento>	MementoUndo		= new ArrayList<>();
	private int index = 0;
	
	public History() {
		OriginatorRedo 	= new ArrayList<>(); 
		MementoRedo	 	= new ArrayList<>();
		OriginatorUndo 	= new ArrayList<>();
		MementoUndo		= new ArrayList<>();
	}
	
	public void checkBranch() {
		if(index < OriginatorRedo.size()) {
			ArrayList<HistoryOriginator> tmpRedoOri 	= new ArrayList<>();
			ArrayList<HistoryMemento> tmpRedoMem 	= new ArrayList<>();
			ArrayList<HistoryOriginator> tmpUndoOri 	= new ArrayList<>();
			ArrayList<HistoryMemento> tmpUndoMem 	= new ArrayList<>();
			for(int i = 0; i < index + 1; i++) {
				tmpRedoOri.add(OriginatorRedo.get(i));
				tmpRedoMem.add(MementoRedo.get(i));
				tmpUndoOri.add(OriginatorUndo.get(i));
				tmpUndoMem.add(MementoUndo.get(i));
			}
			this.OriginatorRedo = tmpRedoOri;
			this.MementoRedo 	= tmpRedoMem;
			this.OriginatorUndo	= tmpUndoOri;
			this.MementoUndo		= tmpUndoMem;
		} 
		index+=1;
	}
	
	

	public void redo() {	
		if(index < OriginatorRedo.size()) {
			OriginatorRedo.get(index).restoreFromHistoryMemento(MementoRedo.get(index));
			index+=1;
		}
	}

	public void undo() {	
		if(index > 0) {
			index-=1;
			OriginatorUndo.get(index).restoreFromHistoryMemento(MementoUndo.get(index));
		}
	}

	public void addRedoCommand(HistoryOriginator h, HistoryMemento hm) {
		this.OriginatorRedo.add(h);
		this.MementoRedo.add(hm);
	}
	
	public void addUndoCommand(HistoryOriginator h, HistoryMemento hm) {
		this.OriginatorUndo.add(h);
		this.MementoUndo.add(hm);
	}
}
