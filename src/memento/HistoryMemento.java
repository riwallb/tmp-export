package memento;

public class HistoryMemento implements Memento{
	public int leftBound, rightBound;
	public String text;
	
	public HistoryMemento(int leftBound, int rightBound, String text) {
		this.leftBound = leftBound;
		this.rightBound = rightBound;
		this.text = text;
	}
	
	public int getLeftBound() { return this.leftBound; }
	public int getRightBound() { return this.rightBound; }
	public String getText() { return this.text; }
}
