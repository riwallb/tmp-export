package memento;

public interface HistoryOriginator {
	public HistoryMemento saveToHistoryMemento();
	public HistoryMemento generateNemesisMemento();
	public void restoreFromHistoryMemento(HistoryMemento m);
}
