package memento;

public class Interval {
	private int leftBound;
	private int rightBound;
	
	public Interval(int leftBound, int rightBound) {
		this.leftBound = leftBound;
		this.rightBound = rightBound;
	}
	
	public int getLeftBound() {
		return this.leftBound;
	}
	
	public int getRightBound() {
		return this.rightBound;
	}
}
