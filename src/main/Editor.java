package main;

import ihm.FrameController;

public class Editor {

	public static void main(String[] args) {
		FrameController frameControler = new FrameController();
		frameControler.configure();
		frameControler.showIHM();
	}
	
}
