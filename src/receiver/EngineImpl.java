package receiver;

import java.util.ArrayList;

import ihm.FrameController;
import singleton.OptionData;

public class EngineImpl implements Engine {
	
	private FrameController	frameController;
	private int index = 0;
	
	private String mainBuffer 	= "";
	private String clipboard		= "";
	private int selectBack		= 0;
	private int selectFront		= 0;
	
	boolean	accPressed	= false;		// Enable special treatment after accent pressed

	public void cut() throws UnsupportedOperationException {
		copy();
		updateText("\\DEL");
	}
	
	public void copy() throws UnsupportedOperationException {
		clipboard = getTextBetweenSelection();
	}
	
	public void paste() throws UnsupportedOperationException {
		updateText(clipboard);
	}
	
	public void select(int front, int back) throws UnsupportedOperationException {
		this.selectFront = front;
		this.selectBack  = back;
		if(this.selectFront < 0)
			this.selectFront = 0;
		if(this.selectFront > mainBuffer.length())
			this.selectFront = mainBuffer.length();
		if(this.selectBack < 0)
			this.selectBack = 0;
		if(this.selectBack > mainBuffer.length())
			this.selectBack = mainBuffer.length();
	}
	
	public void insert(String text) throws UnsupportedOperationException {
		if( mask_extendedPrintableCharacter(text) ) {	
			if ( accPressed ) {
				accPressed 	= false;
				if ( !OptionData.accents.contains(text) ) {
					selectBack 	= selectFront;
					updateText("\\DEL");
				}
			} 
			if ( OptionData.accents.contains(text) ) 
				accPressed 	= true;	
			updateText(text);
		} else if(!text.isEmpty()){
			switch (text.charAt(0)) {
				case 	8	: 	updateText("\\DEL");
							break;
				case		9	: 	updateText("\t");
							break;
				case		13	:	// 13 == shift + entry / 10 == entry
				case 	10	: 	updateText("\n");
							break;
				default		: System.out.println("Caractère spécial non pris en charge");
							  System.out.println(text.charAt(0));
			}
		
		}
	}
	
	private boolean mask_extendedPrintableCharacter(String str) {
		if ( str != null && str.length() == 0 ) {
			return true;
		}
		else if ( str != null ) {
			char lastChar = str.charAt(str.length()-1);
			if ( ( str.length() - 1 ) > 0 ) {	
				return (lastChar != 127) && (lastChar > 31) && (lastChar < 256) && mask_extendedPrintableCharacter(str.substring(0, str.length() -1));
			}
			else
				return (lastChar != 127) && (lastChar > 31) && (lastChar < 256);
		}
		return false;
	}
	
	private void	 updateText(String str) {
		StringBuilder	sb 	= new StringBuilder(mainBuffer);
		// DEL managing
		if	( str.equals("\\DEL") ) {
			// DEL managing without selection
			if ( selectFront == selectBack && mainBuffer.length() >= 1 && selectFront > 0) {
				String tmp = sb.substring(selectFront - 1, selectFront);
				sb.replace(selectFront - 1, selectBack, "");
				selectBack 			= ( --selectFront );
			// DEL managing with selection
			} else {
				String tmp = sb.substring(selectFront, selectBack);
				sb.replace(selectFront, selectBack, "");
				selectBack 			= ( selectFront );
			}
		}
		// character insertion managing without selection
		else if ( selectFront == selectBack ) {
			int tmp = selectFront;
			sb.insert(selectFront, str);
			selectBack 			= ( selectFront+=str.length() );
		}
		// character insertion managing with selection
		else {
			String tmp = sb.substring(selectFront, selectBack);
			int tmpFront = selectFront;
			sb.replace(selectFront, selectBack, str);
			selectBack 			= ( selectFront+=str.length() );
		}
		mainBuffer			= sb.toString();
		refresh();
	}
	
	private String getTextBetweenSelection() {
		StringBuilder sb = new StringBuilder(mainBuffer);
		if(selectFront > sb.length())
			selectFront = (sb.length());
		if(selectBack > sb.length())
			selectBack = (sb.length());
		// \n managing for copy/cut a new line
		int tmp = (selectFront >= 2 ? selectFront : 2);
		if ( sb.length() >= 2 && sb.charAt(tmp - 1) == 'n' && sb.charAt(tmp - 2) == '\\' )
			selectFront-=2;
		return sb.substring(selectFront, selectBack);
	}

	private void refresh() {
		frameController.updateIHM(mainBuffer);
	}
	
	public void setControler(FrameController frameController) {
		this.frameController = frameController;
	}
	
	public void	 updateTextEngine(String str) {
		StringBuilder	sb 	= new StringBuilder(mainBuffer);
		// DEL managing
		if	( str.equals("\\DEL") ) {
			// DEL managing without selection
			if ( selectFront == selectBack && mainBuffer.length() >= 1 && selectFront > 0) {
				sb.replace(selectFront - 1, selectBack, "");
				selectBack 			= ( --selectFront );
			// DEL managing with selection
			} else {
				sb.replace(selectFront, selectBack, "");
				selectBack 			= ( selectFront );
			}
		}
		// character insertion managing without selection
		else if ( selectFront == selectBack ) {
			sb.insert(selectFront, str);
			selectBack 			= ( selectFront+=str.length() );
		}
		// character insertion managing with selection
		else {
			sb.replace(selectFront, selectBack, str);
			selectBack 			= ( selectFront+=str.length()-1 );
		}
		mainBuffer			= sb.toString();
		refresh();
	}
	
	public void updateSelectEngine(int front, int back) throws UnsupportedOperationException {
		this.selectFront = front;
		this.selectBack  = back;
	}
	
	@Override
	public String getTextBetween(int selectFront, int selectBack) {
		return mainBuffer.substring(selectFront, selectBack);
	}
	
	// Getters
	public FrameController getFrameController() { return frameController; }
	public int getIndex() { return index; }
	public String getMainBuffer() { return mainBuffer; }
	public String getClipboard() { return clipboard; }
	public int getSelectBack() { return selectBack; }
	public int getSelectFront() { return selectFront; }
	public boolean isAccPressed() { return accPressed; }
}
