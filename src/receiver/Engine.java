package receiver;

public interface Engine {
	abstract public void cut();
	abstract public void copy();
	abstract public void paste();
	abstract public void select(int front, int back);
	abstract public void insert(String text);
	abstract public int getSelectBack();
	abstract public int getSelectFront();
	abstract public String getClipboard();
	abstract public String getTextBetween(int selectFront, int selectBack);
}
