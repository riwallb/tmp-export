package singleton;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OptionData {
	// Will change in the future 
		public static final int COPY_VALUE_MACRO 		= 3;		// Ctrl + C
		public static final int PASTE_VALUE_MACRO 		= 22;	// Ctrl + V
		public static final int CUT_VALUE_MACRO			= 24;	// Ctrl + X
		public static final int SELECT_ALL_VALUE_MACRO 	= 1;		// Ctrl + A
		public static final int NEXT_FOCUS_VALUE_MACRO 	= 9;		// Ctrl + Tab
		public static final int UNDO_VALUE_MACRO 		= 26;	// Ctrl + Z
		public static final int REDO_VALUE_MACRO 		= 18;	// Ctrl + R
		
		// could be implemented
		public static final int SAVE_VALUE_MACRO	= 0;
		public static final int FIND_VALUE_MACRO = 0;
		// etc.
		
		// Special characters : accents
		public static final List<String> accents 		= Collections.unmodifiableList(Arrays.asList("^", "¨", "`", "~"));
		public static final List<String> letterAccents 	= Collections.unmodifiableList(Arrays.asList("Ü","É","Â","Ä","À",
																									"a accent aigu",
																									"Ê","Ë","È","É","Ï",
																									"Î","Ì","Ô","Ö","Õ",
																									"Ò","Ñ"));
}
