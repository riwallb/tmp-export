package command;

import memento.CareTaker;

public class Start implements Command {
	private CareTaker caretaker;
	
	public Start(CareTaker caretaker) {
		this.caretaker = caretaker;
	}

	@Override
	public void execute() {
		this.caretaker.startRecord();
	}

}
