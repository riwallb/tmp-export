package command;

import memento.History;
import receiver.Engine;

public class Redo implements Command {
	private History history;
	
	public Redo(History history) {
		this.history = history;
	}
	
	public void execute() {
		this.history.redo();
	}

}