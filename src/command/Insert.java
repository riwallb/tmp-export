package command;

import ihm.FrameController;
import memento.CareTaker;
import memento.History;
import memento.HistoryMemento;
import memento.InsertMemento;
import memento.Memento;
import memento.Originator;
import memento.HistoryOriginator;
import receiver.Engine;

public class Insert implements Command, Originator, HistoryOriginator {
	private String		myState;
	private CareTaker	caretaker;
	private Engine 		engine;
	private FrameController	myControler;
	private History		history;

	/**
	 * Insert's constructor taking 4 arguments
	 * @param e : the engine which will make all operations
	 * @param i : the framecontroller which used as intermediary 
	 * @param caretaker : the caretaker which will save command's mementos for macros
	 * @param history : the history which will save command's mementos for undo/redo
	 */
	public Insert (Engine e, FrameController i, CareTaker caretaker, History history) {
		this.engine = e;
		this.myControler	= i;
		this.caretaker = caretaker;
		this.history = history;
	}
	
	/**
	 * Execute method of the Command's interface
	 * If the command is executed by a macro, we don't get the insert string from the controler 
	 * Call history's functions to manage the undo / redo tree and save history memento from insert into history
	 * Call the insert function of engine and record it in the caretaker if the caretaker is recording
	 */
	public void execute() {
		// Macro
		if(!this.caretaker.isReplaying())
			myState = this.myControler.getStringFromIHM();
		
		this.history.checkBranch();
		this.history.addRedoCommand(this, this.saveToHistoryMemento());
		this.history.addUndoCommand(this, this.generateNemesisMemento());
		this.engine.insert(myState);		
		
		// Macro
		if(this.caretaker.isRecording())
			this.caretaker.addNewCommand(this);
	}

	/**
	 * saveToMemento returns the memento with the state of Insert command
	 * @return InsertMemento with state 
	 */
	public Memento saveToMemento() {
		InsertMemento insert = new InsertMemento();
		insert.setValue(myState);
		return insert;
	}

	/**
	 * restoreFromMemento set the current command state with the memento value state
	 * @param m : Memento 
	 */
	public void restoreFromMemento(Memento m) {
		InsertMemento i = (InsertMemento) m;
		if(i!=null)
			myState = i.getValue();
	}

	/**
	 * saveToHistoryMemento() returns the memento corresponding of the cut action on the buffer in the engine
	 * @return HistoryMemento instanciated with the current selection and myState, myState is the text which will be inserted
	 */
	public HistoryMemento saveToHistoryMemento() {
		/* We have to save up the current selection (Selection is not concerned about undo/redo) 
		 * and the text which will be inserted */
		return new HistoryMemento(this.engine.getSelectFront(), this.engine.getSelectBack(), myState);
	}

	/**
	 * generateNemesisMemento() returns the memento corresponding of the undo cut action on the buffer in the engine
	 * @return HistoryMemento instanciated with the selection of the inserted text and the old text which will be erased by the insert
	 */
	public HistoryMemento generateNemesisMemento() {
		/* We have to save up the future selection (Selection is not concerned about undo/redo)
		 * and the text which will be erased by this insertion
		 */
		int leftBound 	= this.engine.getSelectFront();
		int rightBound 	= this.engine.getSelectFront()+myState.length();
		return new HistoryMemento(leftBound, rightBound, this.engine.getTextBetween(this.engine.getSelectFront(), this.engine.getSelectBack()));
	}
	
	
	/**
	 * 
	 * @param h : 
	 */
	public void restoreFromHistoryMemento(HistoryMemento h) {
		if(h!=null) {
			this.engine.select(h.getLeftBound(), h.getRightBound());
			this.engine.insert(h.text);
		}
	}
	
}
