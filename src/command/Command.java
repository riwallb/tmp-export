package command;

/**
 * This interface is used for Command Pattern
 */
public interface Command {
	public void execute();
}
