package command;

import ihm.FrameController;
import memento.CareTaker;
import memento.History;
import memento.HistoryMemento;
import memento.HistoryOriginator;
import memento.Memento;
import memento.Originator;
import receiver.Engine;

public class Paste implements Command, Originator, HistoryOriginator {
	private CareTaker caretaker;
	private Engine engine;
	private FrameController myControler;
	private History history;

	public Paste (Engine e, FrameController f, CareTaker caretaker, History history) {
		this.engine = e; 
		this.myControler = f;
		this.caretaker = caretaker;
		this.history = history;
	}
	
	public void execute() {
		this.history.checkBranch();
		this.history.addRedoCommand(this, this.saveToHistoryMemento());
		this.history.addUndoCommand(this, this.generateNemesisMemento());
		this.engine.paste();
		if(this.caretaker.isRecording())
			this.caretaker.addNewCommand(this);
	}

	public Memento saveToMemento() { return null; }
	public void restoreFromMemento(Memento m) {}

	public HistoryMemento saveToHistoryMemento() {
		/* We have to save up the current selection (Selection is not concerned about undo/redo) 
		 * and the text which will be inserted */
		return new HistoryMemento(this.engine.getSelectFront(), this.engine.getSelectBack(), this.engine.getClipboard());
	}

	public HistoryMemento generateNemesisMemento() {
		/* We have to save up the future selection (Selection is not concerned about undo/redo)
		 * and the text which will be erased by this insertion
		 */
		int leftBound 	= this.engine.getSelectFront();
		int rightBound 	= this.engine.getSelectFront()+this.engine.getClipboard().length();
		return new HistoryMemento(leftBound, rightBound, this.engine.getTextBetween(this.engine.getSelectFront(), this.engine.getSelectBack()));
	}

	public void restoreFromHistoryMemento(HistoryMemento h) {
		if(h!=null) {
			this.engine.select(h.getLeftBound(), h.getRightBound());
			this.engine.insert(h.text);
		}
	}
}
