package command;

import memento.CareTaker;
import memento.History;
import memento.HistoryMemento;
import memento.HistoryOriginator;
import memento.Memento;
import memento.Originator;
import receiver.Engine;

public class Cut implements Command, Originator, HistoryOriginator {
	private CareTaker caretaker;
	private Engine engine;
	private History history;

	/**
	  Cut's constructor taking 3 arguments, 
	 * @param e : the engine which will make all operations
	 * @param caretaker : the caretaker which will save command's mementos for macros
	 * @param history : the history which will save command's mementos for undo/redo
	 */
	public Cut (Engine e, CareTaker caretaker, History history) {
		this.engine = e; 
		this.caretaker = caretaker;
		this.history = history;
	}
	
	/**
	 * Execute method of the Command's interface
	 * Call history's functions to manage the undo / redo tree and save history memento from cut into history
	 * Call the cut function of engine and record it in the caretaker if the caretaker is recording
	 */
	public void execute() {
		this.history.checkBranch();
		this.history.addRedoCommand(this, this.saveToHistoryMemento());
		this.history.addUndoCommand(this, this.generateNemesisMemento());
		this.engine.cut();
		if(this.caretaker.isRecording())
			this.caretaker.addNewCommand(this);
	}

	public Memento saveToMemento() {
		return null;
	}

	public void restoreFromMemento(Memento m) {}
	
	/**
	 * saveToHistoryMemento() returns the memento corresponding of the cut action on the buffer in the engine
	 * @return HistoryMemento instanciated with the current selection and "", "" is the result of cut (delete selection)
	 */
	public HistoryMemento saveToHistoryMemento() {
		/* We have to save up the current selection (Selection is not concerned about undo/redo) 
		 * and the text which will be inserted */
		return new HistoryMemento(this.engine.getSelectFront(), this.engine.getSelectBack(), "");
	}

	/**
	 * generateNemesisMemento() returns the memento corresponding of the undo cut action on the buffer in the engine
	 * @return HistoryMemento instanciated with the future selection and the old text which will be erased by the cut
	 */
	public HistoryMemento generateNemesisMemento() {
		/* We have to save up the future selection (Selection is not concerned about undo/redo)
		 * and the text which will be erased by cutting
		 */
		int leftBound 	= this.engine.getSelectFront();
		int rightBound 	= this.engine.getSelectFront();
		return new HistoryMemento(leftBound, rightBound, this.engine.getTextBetween(this.engine.getSelectFront(), this.engine.getSelectBack()));
	}

	/**
	 * restoreFromHistoryMemento restores from history memento its state and calls functions to synchronize engine state with it 
	 * @param h : history memento 
	 */
	public void restoreFromHistoryMemento(HistoryMemento h) {
		if(h!=null) {
			this.engine.select(h.getLeftBound(), h.getRightBound());
			this.engine.insert(h.text);
		}
	}

}
