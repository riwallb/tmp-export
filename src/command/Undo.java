package command;

import memento.History;
import receiver.Engine;

public class Undo implements Command {
	private History history;
	
	public Undo(History history) {
		this.history = history;
	}
	
	public void execute() {
		this.history.undo();
	}

}
