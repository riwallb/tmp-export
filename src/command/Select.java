package command;

import ihm.FrameController;
import memento.CareTaker;
import memento.Interval;
import memento.Memento;
import memento.Originator;
import memento.SelectMemento;
import receiver.Engine;

public class Select implements Command, Originator {
	private CareTaker		caretaker;
	private Interval			myState;
	private Engine			engine;
	private FrameController	myControler;

	public Select (Engine e, FrameController i, CareTaker caretaker) {
		this.engine = e;
		this.myControler	= i;
		this.caretaker = caretaker;
	}
	
	public void execute() {
		if(!this.caretaker.isReplaying())
			myState = new Interval(this.myControler.getFront(), this.myControler.getBack());
	
		this.engine.select(this.myState.getLeftBound(), this.myState.getRightBound());
		if(this.caretaker.isRecording())
			this.caretaker.addNewCommand(this);
	}

	@Override
	public Memento saveToMemento() {
		SelectMemento select = new SelectMemento();
		select.setValue(this.myState);
		return select;
	}

	@Override
	public void restoreFromMemento(Memento m) {
		SelectMemento s = (SelectMemento) m;
		if(s!=null)
			this.myState = s.getValue();
	}

}
