package command;

import memento.CareTaker;
import memento.Memento;
import memento.Originator;
import receiver.Engine;

/**
 * Implementation class of the command Copy implementing Command and Originator interfaces 
 **/
public class Copy implements Command, Originator {
	private Engine		engine;
	private CareTaker	caretaker;

	/**
	 * Copy's constructor taking 2 arguments
	 * @param e : the engine which will make all operations
	 * @param caretaker : the caretaker which will save command's mementos for macros 
	 */
	public Copy (Engine e, CareTaker caretaker) {
		this.engine = e;
		this.caretaker = caretaker;
	}
	
	/**
	 * Execute method of the Command's interface
	 * Call the copy function of engine and record it in the caretaker if the caretaker is recording
	 */
	public void execute() {
		this.engine.copy();
		if(this.caretaker.isRecording())
			this.caretaker.addNewCommand(this);
	}
	
	public Memento saveToMemento() {
		return null;
	}

	public void restoreFromMemento(Memento m) {
		
	}

}
