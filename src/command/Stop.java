package command;

import memento.CareTaker;

public class Stop implements Command {
	private CareTaker caretaker;
	
	public Stop(CareTaker caretaker) {
		this.caretaker = caretaker;
	}

	@Override
	public void execute() {
		this.caretaker.stopRecord();
	}
}
