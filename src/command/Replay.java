package command;

import memento.CareTaker;

public class Replay implements Command{
	private CareTaker caretaker;
	
	public Replay(CareTaker caretaker) {
		this.caretaker = caretaker;
	}

	@Override
	public void execute() {
		this.caretaker.replay();
	}
}
