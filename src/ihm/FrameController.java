package ihm;

import command.Copy;
import command.Cut;
import command.Insert;
import command.Paste;
import command.Redo;
import command.Replay;
import command.Select;
import command.Start;
import command.Stop;
import command.Undo;
import memento.CareTaker;
import memento.History;
import receiver.EngineImpl;


public class FrameController {
	
	private Frame frame;
	private EngineImpl engine;
	private CareTaker caretaker;
	private History history;
	
	public FrameController () {
		this.engine 	= new EngineImpl();
		this.frame 	= new Frame();
		this.caretaker  = new CareTaker();
		this.history	 = new History();
	}

	public String getStringFromIHM() {
		return frame.getText();
	}
	
	public void configure() {
		this.frame.addCommand("undo", 	new Undo(this.history));
		this.frame.addCommand("redo", 	new Redo(this.history));
		this.frame.addCommand("copy",    new Copy(this.engine, this.caretaker));
		this.frame.addCommand("cut", 	new Cut(	this.engine, this.caretaker, this.history));
		this.frame.addCommand("paste", 	new Paste(this.engine, this, this.caretaker, this.history));
		this.frame.addCommand("insert", 	new Insert(this.engine, this, this.caretaker, this.history));
		this.frame.addCommand("select", 	new Select(this.engine, this, this.caretaker));
		this.frame.addCommand("start",	new Start(this.caretaker));
		this.frame.addCommand("stop",    new Stop(this.caretaker));
		this.frame.addCommand("replay",  new Replay(this.caretaker));
		this.engine.setControler(this);
	}
	 
	public void showIHM() {
		this.frame.setVisible(true);
	}
	
	public void updateIHM(String buffer) {
		frame.updateIHM(buffer);
	}
	// Getters & Setters
	public int getFront() { return frame.getFront(); }
	public int getBack() { return frame.getBack(); }
	public EngineImpl getEngine() { return engine; }
	public Frame getFrame() { return frame; }
	public CareTaker getCareTaker() { return caretaker; }
}
