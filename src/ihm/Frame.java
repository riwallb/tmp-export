package ihm;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import command.Command;
import memento.CareTaker;
import memento.Memento;
import memento.Originator;
import singleton.OptionData;

@SuppressWarnings("serial")
public class Frame extends JFrame implements KeyListener, MouseListener, ActionListener {
	
	private JMenuBar 			menuBar;
	private JTextPane			textPane;
	private JScrollPane			scrollPane;
	private JMenuItem			saveMacro;
	private JMenuItem			stopMacro;
	private JMenuItem			replayMacro;
	
	private Map<String,Command> 	myCommands;	// Map containing all possible commands obtainable through string key
	private int					front;		// Beginning index of the selection
	private int 					back;		// End index of the selection
	
	private Set<Character>		keysPressed;	// Save up all characters pressed with control
	private boolean				listenMacro;	// Enable listening macro when control is pressed
	private char 				text;		// Value of the pressed key
	
	public Frame() {
		this.text 			= 0;
		this.listenMacro 	= false;
		this.keysPressed 	= new HashSet<>();
		this.myCommands  	= new HashMap<String,Command>();
		createMenu();			
		createMainContent();
		this.pack();
		this.setVisible(false);
		this.setMinimumSize(new Dimension(300, 300));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void createMenu() {
		menuBar = new JMenuBar();	
		JMenu menu = new JMenu("File");
		JMenuItem 	menuItem;	
		menuItem = new JMenuItem("New");
		menu.add(menuItem);
		menuItem = new JMenuItem("Open");
		menu.add(menuItem);
		menuItem = new JMenuItem("Save");
		menu.add(menuItem);
		menuItem = new JMenuItem("Save under...");
		menu.add(menuItem);
		menuItem = new JMenuItem("Close");
		menu.add(menuItem);
		menuBar.add(menu);
		menu = new JMenu("Edition");
		menuItem = new JMenuItem("Démarrer enregistrement macro");
		menuItem.addActionListener(this);
		menu.add(menuItem);
		saveMacro = menuItem;
		menuItem = new JMenuItem("Stopper enregistrement macro");
		menuItem.addActionListener(this);
		menuItem.setEnabled(false);
		menu.add(menuItem);
		stopMacro = menuItem;
		menuItem = new JMenuItem("Jouer la macro");
		menuItem.addActionListener(this);
		menuItem.setEnabled(false);
		menu.add(menuItem);
		replayMacro = menuItem;
		menuBar.add(menu);
		this.setJMenuBar(this.menuBar);
	}
	
	public void createMainContent() {
		this.textPane 			= new JTextPane();
		this.textPane.setText("");
		this.textPane.setEditable(false);
		this.textPane.addKeyListener(this);
		this.textPane.addMouseListener(this);
		this.textPane.setFocusTraversalKeysEnabled(false);
		
		this.scrollPane			= new JScrollPane(textPane);
		this.getContentPane().add(this.scrollPane);
	}

	// Macro managing - listeners
	@Override
	public void keyPressed(KeyEvent arg0) {
		if( arg0.getKeyCode() == KeyEvent.VK_CONTROL) 
			listenMacro = true;
		else if( listenMacro ) {
			keysPressed.add(arg0.getKeyChar());
			callCommand();
		}
		else if( arg0.getKeyCode() == KeyEvent.VK_RIGHT ) {
			back = (++front);
			myCommands.get("select").execute();
		}
		else if ( arg0.getKeyCode() == KeyEvent.VK_LEFT) {
			back = (--front);
			myCommands.get("select").execute();
		} else {
			text = arg0.getKeyChar();
			myCommands.get("insert").execute();
		}
	}
	
	@Override
	public void keyReleased(KeyEvent arg0) {
		if( arg0.getKeyCode() == KeyEvent.VK_CONTROL ) 
			listenMacro = false;
	}
	

	@Override
	public void mousePressed(MouseEvent arg0) {
		this.textPane.setCaretPosition(textPane.viewToModel2D(arg0.getPoint()));
		setFirstIndice(this.textPane.getCaretPosition());
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		this.textPane.setCaretPosition(textPane.viewToModel2D(arg0.getPoint()));
		setLastIndice(this.textPane.getCaretPosition());
		orderIndices();
		myCommands.get("select").execute();
	}
	

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JMenuItem menuItem = (JMenuItem) arg0.getSource();
		switch (menuItem.getText()) {
		case "Démarrer enregistrement macro":	menuItem.setEnabled(false);
												stopMacro.setEnabled(true);
												replayMacro.setEnabled(false);
												myCommands.get("start").execute();				
												break;
												
		case "Stopper enregistrement macro":		menuItem.setEnabled(false);
												saveMacro.setEnabled(true);
												replayMacro.setEnabled(true);
												myCommands.get("stop").execute();			
												break;
												
		case "Jouer la macro":					myCommands.get("replay").execute();			
												break;
		default:
		}
	}
	
	public void updateIHM(String buffer) {
		this.textPane.setText(buffer);
	}
	
	public void addCommand(String key, Command command) {
		myCommands.put(key, command);
	}
	
	public void callCommand() {
		Iterator<Character> iterator = keysPressed.iterator();
		while( iterator.hasNext() ) {
				switch( Integer.valueOf( iterator.next() ) ) {
				case	 OptionData.NEXT_FOCUS_VALUE_MACRO	: 	this.transferFocus();					
				break;
				case OptionData.SELECT_ALL_VALUE_MACRO 	: 	setFirstIndice(0); 
				  											setLastIndice(this.textPane.getText().length());
				  											this.myCommands.get("select").execute(); 	
				break;	
				case OptionData.COPY_VALUE_MACRO 		: 	this.myCommands.get("copy").execute(); 	
				break;
				case OptionData.PASTE_VALUE_MACRO 		: 	this.myCommands.get("paste").execute();	
				break;
				case OptionData.CUT_VALUE_MACRO 			: 	this.myCommands.get("cut").execute();		
				break;
				case OptionData.UNDO_VALUE_MACRO			:	this.myCommands.get("undo").execute();
				break;
				case OptionData.REDO_VALUE_MACRO			:	this.myCommands.get("redo").execute();
				break;
				default	: System.out.println("Macron non pris en charge"); 							
				break;
			}
		}
		keysPressed.clear();
	}
		
	public void orderIndices() {
		if ( this.front > this.back ) {
			int tmp = this.back;
			this.back = this.front;
			this.front = tmp;
		}
	}
	
	
	// Getters & Setters
	public int getFront() 	{ return this.front; }
	public int getBack() 	{ return this.back; }
	public String getText() 	{ return String.valueOf(this.text); }
	public void setText(char c) { this.text = c; }
	public void setFirstIndice(int firstIndice) 	{ this.front = firstIndice; }
	public void setLastIndice(int lastIndice) 	{ this.back = lastIndice; }
	public Map<String,Command> getCommands()		{ return this.myCommands; }


	// -------------	Useless -------------- //
	@Override
	public void keyTyped(KeyEvent arg0) {}
	@Override
	public void mouseClicked(MouseEvent arg0) {}
	// Useless
	@Override
	public void mouseEntered(MouseEvent arg0) {}
	// Useless
	@Override
	public void mouseExited(MouseEvent arg0) {}

}
